import socket

# On crée l'interface.
interface = socket.socket(socket.AF_INET6)

# On enregistre l'adresse de notre machine.
hôte = socket.gethostname()
# On définit un port sur lequel écoutera l'interface
port = 8898

# On lie l'interface à notre machine et on dit que l'interface doit
# écouter sur le port donné en paramètre.
interface.bind((hôte, port))

# Permet de mettre en attente de connexion une interface. L'interface
# ne peut avoir plus de cinq connexions en attente avec le code
# suivant.
interface.listen(5)

while True:
    # client contiendra une interface pour discuter avec le client et
    # adresse contiendra l'adresse du client.
    client,adresse = interface.accept ()

    print("Une connexion venant de " + str(adresse) + ".")

    # On lit ce que le client nous envoit.
    données = client.recv(1024)

    # On l'affiche.
    print ("Reçu : " + données.decode("utf-8"))

    # On lui renvoit.
    client.send (données)

    # On ferme la connexion.
    client.close()

interface.close()
