import socket

# On crée l'interface.
interface = socket.socket(socket.AF_INET6)

# On enregistre l'adresse du serveur auquel on souhaite se connecter.
hôte = "::1"
# On définit le port sur lequel écoute le serveur.
port = 8898

# On se connecte au serveur.
interface.connect((hôte, port))

# On envoit un message.
interface.send("Coucou !".encode("utf-8"))

# On ferme la connexion.
interface.close()
